//
//  ScoreDocument.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 05/03/2021.
//

import Foundation

struct ScoreDocument: Codable {
    var guestPenalties: Int
    var guestScore: String
    var hostPenalties: Int
    var hostScore: String
    var id: String
}
