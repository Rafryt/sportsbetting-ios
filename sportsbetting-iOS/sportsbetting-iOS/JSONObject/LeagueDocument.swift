//
//  LeagueDocument.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 03/03/2021.
//

import Foundation

struct LeagueDocument: Codable {
    var id: String
    var name: String
    var area: String
    var startDate: String
    var endDate: String
    
//    init(id: String, name: String, area: String, startDate: String, endDate: String) {
//        self.id = id
//        self.name = name
//        self.area = area
//        self.startDate = startDate
//        self.endDate = endDate
//    }
    init(leagueDocument: LeagueDocument) {
        self.id = leagueDocument.id
        self.name = leagueDocument.name
        self.area = leagueDocument.area
        self.startDate = leagueDocument.startDate
        self.endDate = leagueDocument.endDate
    }
}

enum Area: String, Codable {
    case POLAND, ENGLAND, FRANCE, SPAIN, GERMANY, ITALY, EUROPE, UEFA_CHAMPIONS_LEAGUE, EUROPE_LEAGUE
}
