//
//  LeagueDocumentArray.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 04/03/2021.
//

import Foundation

struct LeagueDocumentArray: Codable {
    var array: [LeagueDocument]
}
