//
//  ResponseData.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 08/02/2021.
//

import Foundation

struct JWTToken: Decodable {
    var id_token: String
}
