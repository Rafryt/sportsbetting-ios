//
//  TeamDocument.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 05/03/2021.
//

import Foundation

struct TeamDocument: Codable {
    var id: String
    var leagueId: String
    var name: String
    
    init(teamDocument: TeamDocument) {
        self.id = teamDocument.id
        self.leagueId = teamDocument.leagueId
        self.name = teamDocument.name
    }
}
