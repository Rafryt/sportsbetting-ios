//
//  MatchDocument.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 05/03/2021.
//

import Foundation

struct MatchDocument: Codable {
    var guestTeam: TeamDocument
    var hostTeam: TeamDocument
    var id: String
    var league: LeagueBasicDocument
    var score: ScoreDocument?
    var startDateTime: String
    
    init(matchDocument: MatchDocument) {
        self.guestTeam = matchDocument.guestTeam
        self.hostTeam = matchDocument.hostTeam
        self.id = matchDocument.id
        self.league = matchDocument.league
        self.score = matchDocument.score
        self.startDateTime = matchDocument.startDateTime
    }
}
