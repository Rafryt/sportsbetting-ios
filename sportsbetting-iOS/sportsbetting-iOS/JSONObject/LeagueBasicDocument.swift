//
//  LeagueBasicDocument.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 05/03/2021.
//

import Foundation

struct LeagueBasicDocument: Codable {
    var id: String
    var name: String
}
