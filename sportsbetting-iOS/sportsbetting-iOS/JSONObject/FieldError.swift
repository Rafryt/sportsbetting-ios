//
//  FieldError.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 04/03/2021.
//

import Foundation

struct FieldError {
    var objectName: String
    var field: String
    var message: String
}
