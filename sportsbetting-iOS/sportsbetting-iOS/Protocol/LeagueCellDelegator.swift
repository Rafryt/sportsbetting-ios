//
//  LeagueCellDelegator.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 09/03/2021.
//

import Foundation

protocol LeagueCellDelegator {
    func callMatchesSegueFromCell(tag: Int)
    func callTeamsSegueFromCell(tag: Int)
}
