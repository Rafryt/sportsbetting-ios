//
//  LoginVC.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 05/02/2021.
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var wrongLoginLabel: UILabel!
    
    let apiManager = API()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        if let login = loginTextField.text, let password = passwordTextField.text{
            apiManager.authorize(login: login, password: password) { (success) in
                if success {
                    DispatchQueue.main.async {
                        self.wrongLoginLabel.isHidden = true
                        self.performSegue(withIdentifier: "loginOK", sender: self)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.wrongLoginLabel.isHidden = false
                    }
                }
            }
        }
    }
}
