//
//  LeagueTableViewCell.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 02/03/2021.
//

import UIKit

class LeagueTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leagueNameLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var startDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var matchesButton: UIButton!
    @IBOutlet weak var teamsButton: UIButton!
    
    var delegate: LeagueCellDelegator!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func matchesButtonPressed(_ sender: UIButton) {
        if(self.delegate != nil){
            self.delegate.callMatchesSegueFromCell(tag: self.tag)
        }
    }
    @IBAction func teamsButtonPressed(_ sender: UIButton) {
        if(self.delegate != nil){
            self.delegate.callTeamsSegueFromCell(tag: self.tag)
        }
    }
}
