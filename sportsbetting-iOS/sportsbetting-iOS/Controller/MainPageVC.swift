//
//  MainPageVC.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 05/02/2021.
//

import UIKit

class MainPageVC: UIViewController {

    let apiManager = API()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated: true)
    }

    @IBAction func logOutButtonPressed(_ sender: UIButton) {
        TokenProvider.deleteToken()
        self.navigationController?.popToRootViewController(animated: true)
    }
}
