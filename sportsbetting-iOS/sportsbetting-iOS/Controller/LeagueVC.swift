//
//  LeagueVC.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 11/03/2021.
//

import UIKit

class LeagueVC: UIViewController {
    
    @IBOutlet weak var topSegment: UISegmentedControl!
    @IBOutlet weak var leagueTableView: UITableView!
    
    var apiManager = API()
    var leagueTableArray: [LeagueDocument] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        leagueTableView.dataSource = self
        leagueTableView.register(UINib(nibName: "LeagueTableViewCell", bundle: nil), forCellReuseIdentifier: "LeagueTableViewCell")
        apiManager.getAllActiveLeagues { (leagues) in
            for i in 0..<leagues.count {
                self.leagueTableArray.append(LeagueDocument(leagueDocument: leagues[i]))
            }
            DispatchQueue.main.async {
                self.leagueTableView.reloadData()
            }
        }
    }
    
    @IBAction func topSegmentIsChanged(_ sender: UISegmentedControl) {
        switch topSegment.selectedSegmentIndex {
        case 0:
            leagueTableArray.removeAll()
            apiManager.getAllActiveLeagues { (leagues) in
                for i in 0..<leagues.count {
                    self.leagueTableArray.append(LeagueDocument(leagueDocument: leagues[i]))
                }
                DispatchQueue.main.async {
                    self.leagueTableView.reloadData()
                }
            }
        case 1:
            leagueTableArray.removeAll()
            apiManager.getLeagueNotActivePage { (leagues) in
                for i in 0..<leagues.count {
                    self.leagueTableArray.append(LeagueDocument(leagueDocument: leagues[i]))
                }
                DispatchQueue.main.async {
                    self.leagueTableView.reloadData()
                }
            }
        default:
            break
        }
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMatches" {
            let destinationVC = segue.destination as! MatchTableVC
            destinationVC.leagueId = (sender as! String)
        }
        if segue.identifier == "showTeams" {
            let destinationVC = segue.destination as! TeamTableVC
            destinationVC.leagueId = (sender as! String)
        }
    }
}

// MARK: - Table view data source

extension LeagueVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if leagueTableArray.count == 0 {
            self.leagueTableView.setEmptyMessage("Lista Pusta")
        } else {
            self.leagueTableView.restore()
        }
        return leagueTableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCell", for: indexPath) as! LeagueTableViewCell
        cell.leagueNameLabel.text = leagueTableArray[indexPath.row].name
        cell.countryLabel.text = leagueTableArray[indexPath.row].area
        cell.startDateLabel.text = leagueTableArray[indexPath.row].startDate
        cell.endDateLabel.text = leagueTableArray[indexPath.row].endDate
        cell.tag = indexPath.row
        cell.delegate = self
        return cell
    }
}

//MARK: - LeagueCellDelegator

extension LeagueVC: LeagueCellDelegator {
    func callMatchesSegueFromCell(tag: Int) {
        self.performSegue(withIdentifier: "showMatches", sender: leagueTableArray[tag].id )
        
    }
    func callTeamsSegueFromCell(tag: Int) {
        self.performSegue(withIdentifier: "showTeams", sender: leagueTableArray[tag].id )
    }
}
