//
//  MatchTableViewCell.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 09/03/2021.
//

import UIKit

class MatchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var team1Label: UILabel!
    @IBOutlet weak var team2Label: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
