//
//  MatchTableVC.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 09/03/2021.
//

import UIKit

class MatchTableVC: UITableViewController {
    
    @IBOutlet var matchTableView: UITableView!
    
    var apiManager = API()
    var leagueId: String!
    var matchTableArray: [MatchDocument] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        matchTableView.dataSource = self
        matchTableView.register(UINib(nibName: "MatchTableViewCell", bundle: nil), forCellReuseIdentifier: "MatchTableViewCell")
        apiManager.getAllMatchesInLeague(leagueId: leagueId) { (matches) in
            for i in 0..<matches.count {
                self.matchTableArray.append(MatchDocument(matchDocument: matches[i]))
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if matchTableArray.count == 0 {
            self.tableView.setEmptyMessage("Lista Pusta")
        } else {
            self.tableView.restore()
        }
        return matchTableArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MatchTableViewCell", for: indexPath) as! MatchTableViewCell
        cell.team1Label.text = matchTableArray[indexPath.row].hostTeam.name
        cell.team2Label.text = matchTableArray[indexPath.row].guestTeam.name
        if matchTableArray[indexPath.row].score != nil{
            let score = matchTableArray[indexPath.row].score!
            cell.scoreLabel.text = "\(score.hostScore)-\(score.guestScore)"
        }
        cell.dateLabel.text = matchTableArray[indexPath.row].startDateTime
        
        return cell
    }
}
