//
//  TeamTableVC.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 09/03/2021.
//

import UIKit

class TeamTableVC: UITableViewController {

    @IBOutlet var teamTableView: UITableView!
    
    var apiManager = API()
    var leagueId: String!
    var teamTableArray: [TeamDocument] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        teamTableView.dataSource = self
        teamTableView.register(UINib(nibName: "TeamTableViewCell", bundle: nil), forCellReuseIdentifier: "TeamTableViewCell")
        apiManager.getTeamsByLeague(leagueId: leagueId) { (teams) in
            for i in 0..<teams.count {
                self.teamTableArray.append(TeamDocument(teamDocument: teams[i]))
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if teamTableArray.count == 0 {
            self.tableView.setEmptyMessage("Lista Pusta")
        } else {
            self.tableView.restore()
        }
        return teamTableArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamTableViewCell", for: indexPath) as! TeamTableViewCell
        cell.teamLabel.text = teamTableArray[indexPath.row].name
        return cell
    }
}
