//
//  RegisterVC.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 05/02/2021.
//

import UIKit

class RegisterVC: UIViewController {
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPassTextField: UITextField!
    @IBOutlet weak var registerResultLabel: UILabel!
    
    let apiManager = API()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerResultLabel.isHidden = true
    }
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        if let login = loginTextField.text, let email = emailTextField.text, let password = passwordTextField.text, let password2 = repeatPassTextField.text {
            if password == password2 {
                apiManager.registerAccount(login: login, email: email, password: password) { (result) in
                    DispatchQueue.main.async {
                        self.registerResultLabel.text = result
                        self.registerResultLabel.isHidden = false
                    }
                    
                }
            }
        }
    }
}
