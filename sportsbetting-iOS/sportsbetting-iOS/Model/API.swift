//
//  API.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 08/02/2021.
//

import Foundation

struct API {
    let url = "http://www.sbgame.online"
    let decoder = JSONDecoder()
    
    func authorize(login: String, password: String, completion: @escaping (Bool) -> Void) {
        guard let url = URL(string: self.url + "/api/authenticate"),
              let payload = "{\"password\": \"\(password)\", \"rememberMe\": true, \"username\": \"\(login)\"}".data(using: .utf8) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("*/*", forHTTPHeaderField: "accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = payload
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else { print(error!.localizedDescription); return }
            do{
                let decodedData = try decoder.decode(JWTToken.self, from: data!)
                TokenProvider.setToken(tokenString: decodedData.id_token)
                completion(true)
            } catch {
                completion(false)
                print("Error: \(error)")
            }
        }.resume()
    }
    
    
    func registerAccount(login: String, email: String, password: String, completion: @escaping (String) -> Void) {
        guard let url = URL(string: self.url + "/api/register"),
              let payload = "{ \"email\": \"\(email)\", \"langKey\": \"string\", \"login\": \"\(login)\", \"lowerCaseLogin\": \"\(login.lowercased())\", \"password\": \"\(password)\"}".data(using: .utf8) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("*/*", forHTTPHeaderField: "accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = payload
        var result: String?
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print(error!.localizedDescription)
                result = error!.localizedDescription
                completion(result!)
                return
            }
            result = String(data: data!, encoding: .utf8)
            if result == "" {
                result = "Powodzenie"
            }
            completion(result!)
        }.resume()
    }
    
    func getAllActiveLeagues(completion: @escaping ([LeagueDocument]) -> Void) {
        guard let url = URL(string: self.url + "/api/leagues/all-active") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("*/*", forHTTPHeaderField: "accept")
        request.addValue(TokenProvider.getToken(), forHTTPHeaderField: "Authorization")
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else { print(error!.localizedDescription); return }
            do {
                let decodedData = try decoder.decode([LeagueDocument].self, from: data!)
                completion(decodedData)
            } catch {
                print("Error: \(error)")
            }
        }.resume()
    }
    func getLeagueNotActivePage(completion: @escaping ([LeagueDocument]) -> Void) {
        guard let url = URL(string: self.url + "/api/leagues/not-active") else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("*/*", forHTTPHeaderField: "accept")
        request.addValue(TokenProvider.getToken(), forHTTPHeaderField: "Authorization")
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else { print(error!.localizedDescription); return }
            do {
                let decodedData = try decoder.decode([LeagueDocument].self, from: data!)
                completion(decodedData)
            } catch {
                print("Error: \(error)")
            }
        }.resume()
    }
    
    func getAllMatchesInLeague(leagueId: String, completion: @escaping ([MatchDocument]) -> Void){
        guard let url = URL(string: self.url + "/api/leagues/\(leagueId)/matches/all") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("*/*", forHTTPHeaderField: "accept")
        request.addValue(TokenProvider.getToken(), forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else { print(error!.localizedDescription); return }
            do {
                let decodedData = try decoder.decode([MatchDocument].self, from: data!)
                completion(decodedData)
            } catch {
                print("Error: \(error)")
            }
        }.resume()
    }
    func getTeamsByLeague(leagueId: String, completion: @escaping ([TeamDocument]) -> Void){
        guard let url = URL(string: self.url + "/api/leagues/\(leagueId)/teams/all") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("*/*", forHTTPHeaderField: "accept")
        request.addValue(TokenProvider.getToken(), forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else { print(error!.localizedDescription); return }
            do {
                let decodedData = try decoder.decode([TeamDocument].self, from: data!)
                completion(decodedData)
            } catch {
                print("Error: \(error)")
            }
        }.resume()
    }
}

