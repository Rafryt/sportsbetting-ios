//
//  JWT.swift
//  sportsbetting-iOS
//
//  Created by Rafał Rytel on 12/02/2021.
//

import Foundation

class TokenProvider {
    static var singletonToken: String = ""
    init() {}
    
    static func setToken(tokenString: String) {
        TokenProvider.singletonToken = tokenString
    }
    static func getToken() -> String {
        return "Bearer " + TokenProvider.singletonToken
    }
    static func deleteToken() {
        TokenProvider.singletonToken = ""
    }
}
